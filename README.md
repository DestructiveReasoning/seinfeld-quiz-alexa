# Seinfeld Quote Trivia
This is an Alexa Skill that presents Seinfeld Quotes, and the user guesses who
said them.

This is a **work in progress** - as such, the code includes various debugging
implements.

## Dependencies

* Apache Maven
* You will need an AWS account and an Amazon Developer account (both free)

An AWS account will be needed to host the Alexa Skill's backend in an AWS Lambda
function (and optionally for the DynamoDB if you choose). AWS Lambda is free to
use for up to 1 million requests per month. Moreover, Amazon DynamoDB is free to
use for up to 25G of storage per month. The Developer account is needed to
create the Alexa Skill's Invocation Model, and is free.

## Setup Guide

### Set up the Database (Optional)

Optionally, you can set up a DynamoDB to monitor statistics about correct
guesses for the quotes in the game. When this is set up, Alexa will inform the
user with the percentage of previous users that had guessed the author of the
past quote correctly. Furthermore, you can use the database for creating your
own metrics. The database does not store any sensitive data - each record is
made up of a quote, the amount of correct guesses for that quote, and the amount
of total guesses for that quote.

If you'd like to use DynamoDB to monitor statistics about correct guesses for
the various Seinfeld quotes, the database should be set up first. Log into the
AWS console and navigate to DynamoDB. Create a new table named
"seinfeld-quote-guess-stats", and name the primary key "quote" with type String.
Make note of the ARN for this table.

Next, navigate back to the AWS console and select IAM. Create a new IAM policy
with read and write permissions to the "seinfeld-quote-guess-stats" table you
just created. This policy will be attached to the role used for the AWS Lambda
function later on.

### Build the Jar

First, open the `pom.xml` file and configure the property that determines if the
skill should connect to a database. This is done by setting the `usedynamo`
property in the `pom.xml`'s `properties` section accordingly. For example, the
following example enables the database:

```xml
<properties>
  <usedynamo>true</usedynamo>
</properties>
```

The Jar file for the backend must now be built. This Jar will be uploaded to
a Lambda function. To build the jar, execute the following:
```
$ git clone https://gitlab.com/DestructiveReasoning/seinfeld-quiz-alexa
$ cd seinfeld-quiz-alexa
$ mvn package
```
The `package` target will generate two Jars in the `target` directory. The one
that is suffixed by `-jar-with-dependencies.jar` is the one to be uploaded to
the Lambda function.

### Set up the Alexa Skill Model

Log in to your Amazon Developer account and start a new Alexa Skill, with
whichever name you choose. The invocation name can also be whatever you want,
but should be at least two words. I use `larry david`.

Next, you must create at least two intents. The first one, named
`SeinfeldQuizIntent`, tells Alexa to present the user with a new quote. Enter
some sample utterances, these will be used to tell Alexa what the user should
say to trigger this intent. Some examples that I use are "new quote" and "new
line". Then, I may say "Alexa, ask larry david for a new quote" to trigger this
intent.

The next intent you must specify is named `AuthorGuessIntent`, which captures
the user's guess of who said the previous quote. In order to place the user's
guess in a variable, we must create a *slot*, which must be named `guess`. This
is done by including the term `{guess}` in utterances where you expect the user
to specify an author name. An utterance that I use is "that was {guess}". After
creating the slot, you must specify the slot type. I use `AMAZON.US_FIRST_NAME`,
which has proven to be moderately good, but has trouble recognizing names
sometimes.

Optionally, you can define another intent named `AuthorRepeatIntent`, which
simply asks Alexa to say the name of the author of the previous quote instead of
having to guess it. Again, define some utterances if you choose to create this
intent.

Click on the "Endpoint" tab on the nav bar on the left side of the page, and
copy the Alexa Skill ID.

Finally, save and build the model.

### Create and Configure the Lambda Function

Log in to the AWS console, and navigate to AWS Lamdba. Create a new function in
the US East 1 region (North Virginia). Name your function whatever you'd like.
For permissions, choose "create custom role", and click "allow" on the page it
brings you to. Furthermore, choose the Java 8 runtime, as the backend is written
in Scala. Leave the rest at their default settings.

On the next page, choose "Upload a .ZIP or JAR file" in the "Code entry type"
dropdown. Click the upload button, and upload the Jar file designated
previously. In the "Handler" box, type
`com.destructivereasoning.seinfeldskill.StreamHandler`. Next, in the designer
box towards the top of the page, add "Alexa Skills Kit" as a trigger for the
Lambda function. Paste the Alexa Skill ID from before in the "Skill ID" box.

Find the function's ARN at the top of the page, and copy it.

Finally, click "Save" and make sure no errors are given.

If you chose to set up the database, navigate to the IAM console, select
"Roles", and find the IAM role you just created for the AWS Lambda function (if
you didn't change any settings, this role is likely called
"lambda_basic_execution"). Click "attach policies", and select the IAM policy
you made for read/write access to DynamoDB in the first step.

### Link the Lambda Function to the Alexa Skill

Navigate back to your Alexa Skill Builder page, and click on the "Endpoint" tab.
In the "Default Region" box, paste the ARN copied in the previous step. Then,
click "Save Endpoints", and build the model again.

Now, your skill should be ready to use.

### Using the Skill on your Alexa-Enabled Device (Optional)

If you'd like to test the skill on your own Alexa-enabled device, such as an
Amazon Echo, one more step is required. In the Alexa Skill Builder page, click
the "Test" tab in the nav bar at the top of the page. Turn on the "Test is
Enabled for this Device" switch. You may also need to enable the skill in your
Alexa app's skills page.

## Usage

The game begins by asking Alexa for a new quote, by triggering the
`SeinfeldQuizIntent` with the utterances you defined previously. Alexa will spit
out a Seinfeld quote and wait for the user's guess. The user makes a guess by
triggering the `AuthorGuessIntent` and specifying a character name in the
`guess` slot. Alexa then tells you if your guess is correct. Please find the
samples interactions below, which use the utterances and invocation name that I
defined in my Alexa skill:

```
User: Alexa, ask Larry David for a new quote
Alexa: 'The sea was angry that day my friends, like an old man trying to send
back soup in a deli'
User: That was George.
Alexa: Correct.

User: Alexa, ask Larry David for a new quote
Alexa: 'You know what they say - you don't sell the steak, you sell the sizzle'
User: That was Jerry
Alexa: Incorrect. The correct answer is Kramer.

User: Alexa, ask Larry David for a new quote
Alexa: 'Do you ever just get down on your knees and thank God that you know me
and have access to my dementia?'
User: That was George Costanza
Alexa: Correct.
```

## Next Steps

* Try to add some presets to allow Alexa to recognize Seinfeld character names
better.
* Change Alexa's response when a user responds something like "I don't know who
said that" in response to a quote. Currently, Alexa just responds "Incorrect.
The correct answer is...".
* Better error handling to remove generic Alexa failure responses.
