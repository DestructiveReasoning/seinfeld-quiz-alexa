package com.destructivereasoning.seinfeldskill

import org.scalatest.Assertions
import org.junit.Test
import org.junit.Assert

@Test
class TestResponseJudgements extends Assertions {
  val judge: SeinfeldResponseJudge = new SimpleSeinfeldResponseJudgeImpl

  @Test
  def correctJerryResponseTest {
    judge.judgeResponse("Jerry", "Jerry") match {
      case IncorrectResponse(_, _) => Assert.fail
      case _ => ()
    }
    judge.judgeResponse("Jerry Seinfeld", "jerry") match {
      case IncorrectResponse(_, _) => Assert.fail
      case _ => ()
    }
  }

  @Test
  def correctGeorgeResponseTest {
    judge.judgeResponse("George", "George") match {
      case IncorrectResponse(_, _) => Assert.fail
      case _ => ()
    }
    judge.judgeResponse("George Costanza", "George") match {
      case IncorrectResponse(_, _) => Assert.fail
      case _ => ()
    }
  }

  @Test
  def correctElaineResponseTest {
    judge.judgeResponse("Elaine", "Elaine") match {
      case IncorrectResponse(_, _) => Assert.fail
      case _ => ()
    }
    judge.judgeResponse("Elaine Benes", "Elaine") match {
      case IncorrectResponse(_, _) => Assert.fail
      case _ => ()
    }
    judge.judgeResponse("Laney", "Elaine") match {
      case IncorrectResponse(_, _) => Assert.fail
      case _ => ()
    }
  }

  @Test
  def correctKramerResponseTest {
    judge.judgeResponse("Kramer", "Kramer") match {
      case IncorrectResponse(_, _) => Assert.fail
      case _ => ()
    }
    judge.judgeResponse("Cosmo Kramer", "Kramer") match {
      case IncorrectResponse(_, _) => Assert.fail
      case _ => ()
    }
    judge.judgeResponse("K Man", "Kramer") match {
      case IncorrectResponse(_, _) => Assert.fail
      case _ => ()
    }
  }

  @Test
  def correctArbitraryResponseTest {
    judge.judgeResponse("Yo Yo Ma", "yo yo ma") match {
      case IncorrectResponse(_, _) => Assert.fail
      case _ => ()
    }
  }

  @Test
  def incorrectReponseTest {
    judge.judgeResponse("George Costanza", "Jerry") match {
      case CorrectResponse(_) => Assert.fail
      case _ => ()
    }
  }

  @Test
  def configTest {
    val db = ConfigManager.usingDynamo
    val judge = SeinfeldResponseJudge.getJudge
    if(db) Assert.assertTrue(judge.isInstanceOf[DynamoSeinfeldResponseJudgeImpl])
    else Assert.assertTrue(judge.isInstanceOf[SimpleSeinfeldResponseJudgeImpl])
  }
}
