package com.destructivereasoning.seinfeldskill

class SimpleSeinfeldResponseJudgeImpl extends SeinfeldResponseJudge {
  import SeinfeldResponseJudge._

  override def judgeResponse(response : String, answer : String): ResponseFeedback = {
    super.judgeResponse(normalizeResponse(response), answer)
  }

  private
  def normalizeResponse(response : String): String = {
    val lowerResponse = if(response == null) "" else response.toLowerCase
    if(isJerry(lowerResponse)) JERRY_NORMALIZED
    else if(isGeorge(lowerResponse)) GEORGE_NORMALIZED
    else if(isElaine(lowerResponse)) ELAINE_NORMALIZED
    else if(isKramer(lowerResponse)) KRAMER_NORMALIZED
    else lowerResponse
  }

  private def isJerry(response : String): Boolean = JERRY_ALIASES.contains(response)
  private def isGeorge(response : String): Boolean = GEORGE_ALIASES.contains(response)
  private def isElaine(response : String): Boolean = ELAINE_ALIASES.contains(response)
  private def isKramer(response : String): Boolean = KRAMER_ALIASES.contains(response)
}
