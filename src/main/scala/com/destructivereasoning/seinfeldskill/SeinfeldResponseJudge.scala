package com.destructivereasoning.seinfeldskill

trait SeinfeldResponseJudge {
  def judgeResponse(response : String, answer : String): ResponseFeedback = {
    if(response.toLowerCase.equals(answer.toLowerCase)) CorrectResponse("")
    else IncorrectResponse(answer)
  }

  def judgeResponse(response : String, answer : String, quote : String): ResponseFeedback = {
    judgeResponse(response, answer)
  }
}

abstract class ResponseFeedback
case class CorrectResponse(val comments : String = "")
  extends ResponseFeedback
case class IncorrectResponse(val correctAnswer : String, val comments : String = "")
  extends ResponseFeedback

object SeinfeldResponseJudge {
  val JERRY_NORMALIZED = "jerry"
  val GEORGE_NORMALIZED = "george"
  val ELAINE_NORMALIZED = "elaine"
  val KRAMER_NORMALIZED = "kramer"

  val JERRY_ALIASES = Set(JERRY_NORMALIZED, "jerry seinfeld", "seinfeld")
  val GEORGE_ALIASES = Set(GEORGE_NORMALIZED, "george costanza", "costanza")
  val ELAINE_ALIASES = Set(ELAINE_NORMALIZED, "elaine benes", "laney")
  val KRAMER_ALIASES = Set(KRAMER_NORMALIZED, "cosmo kramer", "cosmo", "k man")

  def getJudge: SeinfeldResponseJudge = {
    if(ConfigManager.usingDynamo) new DynamoSeinfeldResponseJudgeImpl
    else new SimpleSeinfeldResponseJudgeImpl
  }
}
