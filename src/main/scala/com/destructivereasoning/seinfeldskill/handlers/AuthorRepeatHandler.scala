package com.destructivereasoning.seinfeldskill.handlers

import com.amazon.ask.dispatcher.request.handler.HandlerInput
import com.amazon.ask.dispatcher.request.handler.RequestHandler
import com.amazon.ask.model.Response
import com.amazon.ask.request.Predicates

import java.util.Optional

object AuthorRepeatHandler extends SessionRequestHandler {
  override def canHandle(input : HandlerInput): Boolean = {
    input.matches(Predicates.intentName("AuthorRepeatIntent"))
  }

  override def handle(input : HandlerInput): Optional[Response] = {
    val author = input.getAttributesManager().getSessionAttributes.get("author").asInstanceOf[String]
    val comment = if(author == null || author.length == 0) "No quote given yet" else author
    input.getResponseBuilder
       .withSpeech(comment)
       .withSimpleCard("Seinfeld Quote Author Request", comment)
       .withShouldEndSession(true)
       .build
  }
}
