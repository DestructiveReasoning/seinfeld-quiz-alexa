package com.destructivereasoning.seinfeldskill.handlers

import com.amazon.ask.dispatcher.request.handler.HandlerInput
import com.amazon.ask.dispatcher.request.handler.RequestHandler
import com.amazon.ask.model.IntentRequest
import com.amazon.ask.model.Response
import com.amazon.ask.request.Predicates

import java.util.Optional

import com.destructivereasoning.seinfeldskill.{CorrectResponse, IncorrectResponse}
import com.destructivereasoning.seinfeldskill.SeinfeldResponseJudge

object QuizResponseHandler extends SessionRequestHandler {
  val GUESS_KEY_NAME = "guess"

  val judge: SeinfeldResponseJudge = SeinfeldResponseJudge.getJudge

  override def canHandle(input : HandlerInput): Boolean = {
    input.matches(Predicates.intentName("AuthorGuessIntent"))
  }

  override def handle(input : HandlerInput): Optional[Response] = {
    val request: IntentRequest = input.getRequestEnvelope()
                                      .getRequest()
                                      .asInstanceOf[IntentRequest]
    val answer = input.getAttributesManager().getSessionAttributes.get("author").asInstanceOf[String]
    val quote = input.getAttributesManager().getSessionAttributes.get("quote").asInstanceOf[String]
    val response = parseResponse(request)
    println(s"Got response '${response}', answer is '${answer}'")
    val speech = judge.judgeResponse(response, answer, quote) match {
      case CorrectResponse(comments) => s"Correct! ${comments}"
      case IncorrectResponse(answer, comments) =>
        s"That is not correct. The correct answer was ${answer}. ${comments}"
    }
    input.getResponseBuilder
         .withSpeech(speech)
         .withSimpleCard(s"Seinfeld Quote Guess (${response})", speech)
         .withShouldEndSession(true)
         .build
  }

  private
  def parseResponse(request : IntentRequest): String = {
    if(request.getIntent == null) ""
    else if(request.getIntent().getSlots == null) ""
    else if(request.getIntent().getSlots().get(GUESS_KEY_NAME) == null) ""
    else request.getIntent().getSlots().get(GUESS_KEY_NAME).getValue
  }
}
