package com.destructivereasoning.seinfeldskill.handlers

import com.amazon.ask.dispatcher.request.handler.HandlerInput
import com.amazon.ask.dispatcher.request.handler.RequestHandler
import com.amazon.ask.model.Response
import com.amazon.ask.request.Predicates

import java.util.Optional

import scala.util.parsing.json._

object QuizIntentHandler extends SessionRequestHandler {
  val QUOTE_ENDPOINT = "https://seinfeld-quotes.herokuapp.com/random"
  val unknownJson = "{\"quote\":\"\", \"author\":\"\"}"

  override def canHandle(input : HandlerInput): Boolean = {
    input.matches(Predicates.intentName("SeinfeldQuizIntent"))
  }

  override def handle(input : HandlerInput): Optional[Response] = {
    val quoteMap = getQuoteMap
    val author = quoteMap.get("author").map(x => if(x.isEmpty) "Anonymous" else x).getOrElse("Anonymous")
    val quote = quoteMap.get("quote").map(x => if(x.isEmpty) "Nothing" else x).getOrElse("No quote found")
    val comment = quote
    println(s"Echo'd quote: '${quote}' by ${author}")
    withSessionUpdate(input)("quote", quote.asInstanceOf[Object]) {
    withSessionUpdate(input)("author", author.asInstanceOf[Object]) {
      input.getResponseBuilder
         .withSpeech(comment)
         .withSimpleCard("Seinfeld Quiz", comment)
         .withShouldEndSession(false)
         .build
    }}
  }

  private
  def getQuoteMap: Map[String, String] = JSON.parseFull(getQuote)
                                             .getOrElse(unknownJson)
                                             .asInstanceOf[Map[String, String]]

  private
  def getQuote: String = scala.io.Source.fromURL(QUOTE_ENDPOINT).mkString
}
