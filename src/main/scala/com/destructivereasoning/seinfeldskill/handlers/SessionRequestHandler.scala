package com.destructivereasoning.seinfeldskill.handlers

import com.amazon.ask.dispatcher.request.handler.HandlerInput
import com.amazon.ask.dispatcher.request.handler.RequestHandler

trait SessionRequestHandler extends RequestHandler {
  protected
  def withSessionUpdate[T](input : HandlerInput)(key : String, value : Object)(thunk : => T): T = {
    val output = thunk
    var attrs = input.getAttributesManager().getSessionAttributes
    attrs.put(key, value)
    input.getAttributesManager().setSessionAttributes(attrs)
    output
  }
}
