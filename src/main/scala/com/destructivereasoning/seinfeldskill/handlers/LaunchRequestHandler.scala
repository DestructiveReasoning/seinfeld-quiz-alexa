package com.destructivereasoning.seinfeldskill.handlers

import com.amazon.ask.dispatcher.request.handler.HandlerInput
import com.amazon.ask.dispatcher.request.handler.RequestHandler
import com.amazon.ask.model.LaunchRequest
import com.amazon.ask.model.Response
import com.amazon.ask.request.Predicates

import java.util.Optional

object LaunchRequestHandler extends RequestHandler {
  val greeting = "Prettay, prettay, prettay good"

  override def canHandle(input : HandlerInput): Boolean = {
    input.matches(Predicates.requestType(classOf[LaunchRequest]))
  }

  override def handle(input : HandlerInput): Optional[Response] = {
    input.getResponseBuilder()
         .withSpeech(greeting)
         .withSimpleCard("Larry David", greeting)
         .withReprompt(greeting)
         .build
  }
}
