package com.destructivereasoning.seinfeldskill

import handlers.{LaunchRequestHandler, QuizIntentHandler, QuizResponseHandler, AuthorRepeatHandler}

import com.amazon.ask.Skill
import com.amazon.ask.Skills
import com.amazon.ask.SkillStreamHandler

class StreamHandler extends SkillStreamHandler(StreamHandler.getSkill)

object StreamHandler {
  private
  def getSkill: Skill = Skills.standard
                              .addRequestHandlers(LaunchRequestHandler, QuizIntentHandler, QuizResponseHandler, AuthorRepeatHandler)
                              .build
}
