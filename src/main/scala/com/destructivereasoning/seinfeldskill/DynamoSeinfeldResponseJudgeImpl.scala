package com.destructivereasoning.seinfeldskill

import com.amazonaws.services.dynamodbv2.AmazonDynamoDB
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClientBuilder
import com.amazonaws.services.dynamodbv2.model.AttributeValue
import com.amazonaws.services.dynamodbv2.model.GetItemRequest
import com.amazonaws.services.dynamodbv2.model.PutItemRequest
import com.amazonaws.services.dynamodbv2.model.ResourceNotFoundException
import com.amazonaws.services.dynamodbv2.model.UpdateItemRequest

import scala.collection.JavaConverters._

class DynamoSeinfeldResponseJudgeImpl extends SimpleSeinfeldResponseJudgeImpl {
  import SeinfeldResponseJudge._

  val TABLE_NAME = "seinfeld-quote-guess-stats"

  val CORRECT_COL = "correct_guesses"
  val TOTAL_COL = "total_guesses"

  lazy val dbClient: AmazonDynamoDB = AmazonDynamoDBClientBuilder.defaultClient

  override def judgeResponse(response : String, answer : String, quote : String): ResponseFeedback = {
    super.judgeResponse(response, answer) match {
      case CorrectResponse(_) => CorrectResponse(consultDB(quote, true))
      case IncorrectResponse(ans, _) => IncorrectResponse(ans, consultDB(quote, false))
    }
  }

  private
  def consultDB(quote : String, correct : Boolean): String = {
    val itemRequest = new GetItemRequest().withTableName(TABLE_NAME)
                                          .withKey(Map("quote" -> new AttributeValue(quote)).asJava)
    val dbResult = dbClient.getItem(itemRequest).getItem
    if(dbResult == null) processFirstQuotePlay(quote, correct)
    else {
      val curCorrect = dbResult.get(CORRECT_COL).getN().toInt
      val curTotal = dbResult.get(TOTAL_COL).getN().toInt
      processRepeatQuotePlay(quote, correct, DBEntry(quote, curCorrect, curTotal))
    }
  }

  private
  def processRepeatQuotePlay(quote : String, correct : Boolean, curItem : DBEntry): String = {
    val pct = Math.floor(100f * curItem.correctGuesses.toFloat / curItem.totalGuesses).toInt
    val updateRequest = getQuoteUpdateRequest(quote, correct)
    dbClient.updateItem(updateRequest)
    s"This quote was guessed correctly by ${pct} percent of previous players"
  }

  private
  def processFirstQuotePlay(quote : String, correct : Boolean): String = {
    val cor = if(correct) 1 else 0
    val putRequest = new PutItemRequest().addItemEntry("quote", new AttributeValue(quote))
                                         .addItemEntry(TOTAL_COL, new AttributeValue().withN("1"))
                                         .addItemEntry(CORRECT_COL, new AttributeValue().withN(cor.toString))
                                         .withTableName(TABLE_NAME)
    dbClient.putItem(putRequest)
    "You were the first player to play this quote"
  }

  private
  def getQuoteUpdateRequest(quote : String, correct : Boolean): UpdateItemRequest = {
    val cor = if(correct) "1" else "0"
    val attrValues = Map(":cor" -> new AttributeValue().withN(cor),
                         ":tot" -> new AttributeValue().withN("1")).asJava
    new UpdateItemRequest()
        .addKeyEntry("quote", new AttributeValue(quote))
        .withUpdateExpression(s"set ${CORRECT_COL} = ${CORRECT_COL} + :cor, ${TOTAL_COL} = ${TOTAL_COL} + :tot")
        .withExpressionAttributeValues(attrValues)
        .withTableName(TABLE_NAME)
  }
}

case class DBEntry(val quote : String, val correctGuesses : Int, val totalGuesses : Int)
