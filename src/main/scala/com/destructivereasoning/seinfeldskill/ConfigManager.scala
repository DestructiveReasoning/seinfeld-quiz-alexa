package com.destructivereasoning.seinfeldskill

import java.io.InputStream

object ConfigManager {
  private lazy val props = loadProps

  val CONFIG_FILE_PATH = "/my.properties"
  val USING_DYNAMODB_PROPERTY = "usedynamo"

  def getProperty(prop : String): Option[String] = {
    val p = props.getProperty(prop)
    if(p == null) None
    else Some(p)
  }

  lazy val usingDynamo: Boolean = {
    getProperty(USING_DYNAMODB_PROPERTY)
      .map(x => scala.util.Try(x.toBoolean).getOrElse(false))
      .getOrElse(false)
  }

  private
  def loadProps: java.util.Properties = {
    var is: InputStream = null
    try {
      is = getClass.getResourceAsStream(CONFIG_FILE_PATH)
      if(is == null) println("Input stream is null - can't find my.properties?")
      var props = new java.util.Properties
      if(props == null) println("Props is null - couldn't initialize properties")
      props.load(is)
      props
    } finally {
      if(is != null) is.close
    }
  }
}
